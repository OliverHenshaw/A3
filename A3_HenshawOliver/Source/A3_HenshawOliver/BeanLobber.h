// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon.h"
#include "BeanLobber.generated.h"

/**
 * 
 */
UCLASS()
class A3_HENSHAWOLIVER_API ABeanLobber : public AWeapon
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* Body;
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* Spoon;
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* Holder;
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* Can;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
public:
	ABeanLobber();
	// Called every fr
	virtual void Tick(float DeltaTime) override;
};
