// Fill out your copyright notice in the Description page of Project Settings.


#include "SubmachineBean.h"

ASubmachineBean::ASubmachineBean()
{
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	Body = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Body"));
	Body->SetupAttachment(RootComponent);
	Barrel = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Barrel"));
	Barrel->SetupAttachment(Body);
	Can = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Can"));
	Can->SetupAttachment(Body);

	MaxAmmo = 35;
	ProjectileSpeed  = 5400;
	CurrentAmmo = 35;
	FireRate = 0.08;
	ReloadTime = 1.5;
	Magnification = 1.5;
	BaseDamage = 34;
	
	
}

void ASubmachineBean::BeginPlay()
{
	Super::BeginPlay();
}

void ASubmachineBean::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}