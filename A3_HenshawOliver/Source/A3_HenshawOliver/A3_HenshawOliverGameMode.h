// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "A3_HenshawOliverGameMode.generated.h"

UCLASS(minimalapi)
class AA3_HenshawOliverGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AA3_HenshawOliverGameMode();
};



