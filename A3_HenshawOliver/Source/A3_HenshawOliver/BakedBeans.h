// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AmmoType.h"
#include "BakedBeans.generated.h"

/**
 * 
 */
UCLASS()
class A3_HENSHAWOLIVER_API UBakedBeans : public UAmmoType
{
	GENERATED_BODY()
public:
	UBakedBeans();
};
