// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "GameFramework/Actor.h"
#include "AmmoPickup.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE
(FOnAmmoPickup);
UCLASS()
class A3_HENSHAWOLIVER_API AAmmoPickup : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AAmmoPickup();
	UPROPERTY(BlueprintAssignable)
	FOnAmmoPickup AmmoPickupHitBoxDelegate;
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* CanMesh;
	UPROPERTY(EditAnywhere)
	UBoxComponent* AmmoPickupHitBox;
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void OnHitboxOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	                          UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep,
	                          const FHitResult& SweepResult);
};
