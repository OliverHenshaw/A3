// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AmmoType.h"
#include "Cannellini.generated.h"

/**
 * 
 */
UCLASS()
class A3_HENSHAWOLIVER_API UCannellini : public UAmmoType
{
	GENERATED_BODY()
public:
	//UCannellini(){Super(1,0,0,0);}
	UCannellini();
};
