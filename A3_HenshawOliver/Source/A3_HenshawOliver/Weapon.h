// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile.h"
#include "GameFramework/Actor.h"
#include "Weapon.generated.h"

UCLASS()
class A3_HENSHAWOLIVER_API AWeapon : public AActor
{
	GENERATED_BODY()

	
	
	
	
public:	
	// Sets default values for this actor's properties
	AWeapon();
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	//void Fire();
public:
	virtual void Fire();
	virtual void AttachWeapon(AActor* TargetCharacter);
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UPROPERTY(EditAnywhere, Category = Ammo)
	int MaxAmmo;
	UPROPERTY(EditAnywhere, Category = Ammo)
	int CurrentAmmo;
	UPROPERTY(EditAnywhere, Category = Ammo)
	int BaseDamage;
	UPROPERTY(EditAnywhere, Category = Ammo)
	float FireRate;
	UPROPERTY(EditAnywhere, Category = Ammo)
	float ReloadTime;
	UPROPERTY(EditAnywhere, Category = Ammo)
	float Magnification;
	UPROPERTY(EditAnywhere, Category = Ammo)
	int ProjectileSpeed;
	UPROPERTY(EditAnywhere, Category = Ammo)
	TSubclassOf<class UAmmoType> AmmoClass;
	UPROPERTY(EditAnywhere,Category= Ammo)
	TSubclassOf<class AProjectile> ProjectileClass;
	//Getters
	UFUNCTION(BlueprintCallable, Category = Ammo)
	int GetMaxAmmo() const{return MaxAmmo;}
	UFUNCTION(BlueprintCallable, Category = Ammo)
	int GetCurrentAmmo() const{return CurrentAmmo;}
	UFUNCTION(BlueprintCallable, Category = Ammo)
	int GetBaseDamage() const{return BaseDamage;}
	UFUNCTION(BlueprintCallable, Category = Ammo)
	float GetFireRate() const{return FireRate;}
	UFUNCTION(BlueprintCallable, Category = Ammo)
	float GetReloadTime() const{return ReloadTime;}
	UFUNCTION(BlueprintCallable, Category = Ammo)
	float GetMagnification() const{return Magnification;}
	UFUNCTION(BlueprintCallable, Category = Ammo)
	int GetProjectileSpeed() const{return ProjectileSpeed;}

};
