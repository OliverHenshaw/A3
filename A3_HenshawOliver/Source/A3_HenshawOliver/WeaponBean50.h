// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon.h"
#include "WeaponBean50.generated.h"

/**
 * 
 */
UCLASS()
class A3_HENSHAWOLIVER_API AWeaponBean50 : public AWeapon
{
	
	
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* Body;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* Sight;
	UPROPERTY(EditAnywhere)
    	UStaticMeshComponent* Barrel;
	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* Can;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
public:
	AWeaponBean50();
	// Called every fr
	virtual void Tick(float DeltaTime) override;
	/*
	UPROPERTY(EditDefaultsOnly, Category=Projectile)
	TSubclassOf<class AProjectile> ProjectileClass;
	*/

	//AA3_HenshawOliverCharacter* Character;
};
