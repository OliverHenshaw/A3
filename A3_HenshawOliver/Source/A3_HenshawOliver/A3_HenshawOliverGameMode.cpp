// Copyright Epic Games, Inc. All Rights Reserved.

#include "A3_HenshawOliverGameMode.h"
#include "A3_HenshawOliverCharacter.h"
#include "UObject/ConstructorHelpers.h"

AA3_HenshawOliverGameMode::AA3_HenshawOliverGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPerson/Blueprints/BP_FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;
	

}
