// Fill out your copyright notice in the Description page of Project Settings.


#include "Goal.h"

// Sets default values
AGoal::AGoal()
{
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	EndMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh Component"));
	EndMesh->SetupAttachment(RootComponent);
	
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	FinishHitBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Hitbox Component"));
	FinishHitBox->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void AGoal::BeginPlay()
{
	FinishHitBox->OnComponentBeginOverlap.AddDynamic(this, &AGoal::OnHitboxOverlapBegin);
	Super::BeginPlay();
	
}

// Called every frame
void AGoal::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
void AGoal::OnHitboxOverlapBegin
(UPrimitiveComponent* OverlappedComponent,
 AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex,
 bool bFromSweep, const FHitResult& SweepResult)
{
	UE_LOG(LogTemp, Warning, TEXT("Player Overlap"));
	FinishHitBoxDelegate.Broadcast();
}


