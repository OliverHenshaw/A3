// Fill out your copyright notice in the Description page of Project Settings.


#include "AmmoPickup.h"

#include "A3_HenshawOliverCharacter.h"

// Sets default values
AAmmoPickup::AAmmoPickup()
{
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	CanMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh Component"));
	CanMesh->SetupAttachment(RootComponent);
	
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	AmmoPickupHitBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Hitbox Component"));
	AmmoPickupHitBox->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AAmmoPickup::BeginPlay()
{
	Super::BeginPlay();
	AmmoPickupHitBox->OnComponentBeginOverlap.AddDynamic(this, &AAmmoPickup::OnHitboxOverlapBegin);
	Super::BeginPlay();
	
}

// Called every frame
void AAmmoPickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AAmmoPickup::OnHitboxOverlapBegin(UPrimitiveComponent* OverlappedComponent,
 AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex,
 bool bFromSweep, const FHitResult& SweepResult)
{

	AA3_HenshawOliverCharacter* Character = Cast<AA3_HenshawOliverCharacter>(OtherActor);
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Some debug message!"));
	if(Character != nullptr)
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Some debug message!"));
		AmmoPickupHitBoxDelegate.Broadcast();
		Destroy();
		 
	}
	

	
	
}

