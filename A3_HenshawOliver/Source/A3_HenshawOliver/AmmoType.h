// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "AmmoType.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class A3_HENSHAWOLIVER_API UAmmoType : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UAmmoType();
	UAmmoType(int damMultiplier, int slowDown, int DamageTime, int speed);
	UPROPERTY(EditAnywhere)
	int DamageMultiplier;
	UPROPERTY(EditAnywhere)
	int Slowdown;
	UPROPERTY(EditAnywhere)
	int DamageOverTime;
	UPROPERTY(EditAnywhere)
	int SpeedMultiplier;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	//virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
