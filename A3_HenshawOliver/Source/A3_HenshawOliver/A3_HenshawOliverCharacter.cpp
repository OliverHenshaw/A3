// Copyright Epic Games, Inc. All Rights Reserved.

#include "A3_HenshawOliverCharacter.h"
#include "A3_HenshawOliverProjectile.h"
#include "WeaponBean50.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"


//////////////////////////////////////////////////////////////////////////
// AA3_HenshawOliverCharacter

AA3_HenshawOliverCharacter::AA3_HenshawOliverCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	TurnRateGamepad = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(false);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = true;
	Mesh1P->CastShadow = true;
	Mesh1P->SetRelativeRotation(FRotator(1.9f, -19.19f, 5.2f));
	Mesh1P->SetRelativeLocation(FVector(-0.5f, -4.4f, -155.7f));

	
	Ammo = 10;
	Health = 100;
	Armour = 150;

	

}

void AA3_HenshawOliverCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();
	FVector PlayerPosition = GetActorLocation();
	//AWeaponBean50** SpawnedWeapon = (new AWeaponBean50*);
	//GetWorld()->SpawnActor(WeaponClass, &PlayerPosition);
	

}

//////////////////////////////////////////////////////////////////////////// Input

void AA3_HenshawOliverCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);

	// Bind jump events
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	// Bind fire event
	PlayerInputComponent->BindAction("PrimaryAction", IE_Pressed, this, &AA3_HenshawOliverCharacter::OnPrimaryAction);

	//Bind Reload
	PlayerInputComponent->BindAction("Reload", IE_Pressed, this, &AA3_HenshawOliverCharacter::WeaponReload);


	//Binding weapon selection
	PlayerInputComponent->BindAction("Weapon1", IE_Pressed, this, &AA3_HenshawOliverCharacter::Weapon1);
	PlayerInputComponent->BindAction("Weapon2", IE_Pressed, this, &AA3_HenshawOliverCharacter::Weapon2);
	PlayerInputComponent->BindAction("Weapon3", IE_Pressed, this, &AA3_HenshawOliverCharacter::Weapon3);
	PlayerInputComponent->BindAction("Ammo1", IE_Pressed, this, &AA3_HenshawOliverCharacter::Ammo1);
	PlayerInputComponent->BindAction("Ammo2", IE_Pressed, this, &AA3_HenshawOliverCharacter::Ammo2);
	PlayerInputComponent->BindAction("Ammo3", IE_Pressed, this, &AA3_HenshawOliverCharacter::Ammo3);
	// Enable touchscreen input
	EnableTouchscreenMovement(PlayerInputComponent);

	// Bind movement events
	PlayerInputComponent->BindAxis("Move Forward / Backward", this, &AA3_HenshawOliverCharacter::MoveForward);
	PlayerInputComponent->BindAxis("Move Right / Left", this, &AA3_HenshawOliverCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "Mouse" versions handle devices that provide an absolute delta, such as a mouse.
	// "Gamepad" versions are for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn Right / Left Mouse", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("Look Up / Down Mouse", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("Turn Right / Left Gamepad", this, &AA3_HenshawOliverCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("Look Up / Down Gamepad", this, &AA3_HenshawOliverCharacter::LookUpAtRate);
}

void AA3_HenshawOliverCharacter::OnPrimaryAction()
{
	// Trigger the OnItemUsed Event
	OnUseItem.Broadcast();
	//WeaponClass->Fire();
}

void AA3_HenshawOliverCharacter::BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == true)
	{
		return;
	}
	if ((FingerIndex == TouchItem.FingerIndex) && (TouchItem.bMoved == false))
	{
		OnPrimaryAction();
	}
	TouchItem.bIsPressed = true;
	TouchItem.FingerIndex = FingerIndex;
	TouchItem.Location = Location;
	TouchItem.bMoved = false;
}

void AA3_HenshawOliverCharacter::EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == false)
	{
		return;
	}
	TouchItem.bIsPressed = false;
}

void AA3_HenshawOliverCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void AA3_HenshawOliverCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void AA3_HenshawOliverCharacter::WeaponReload()
{
	if (Ammo > 0)
	{
		Ammo -=1;
	}
}

void AA3_HenshawOliverCharacter::Weapon1()
{
	//Bean 50
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("equipped Bean 50"));
	//WeaponClass = &AWeaponBean50();
	//WeaponClass = AttachToActor();
	
		//ConstructObject<UClassOfTheComponent>(UClassOfTheComponent::StaticClass(), GetOwner(), NAME_None, RF_Transient);
    //	WeaponHolder = UChildActorComponent<AWeaponBean50>(TEXT("Bean 50"));
	//FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, true);
	//WeaponHolder->AttachToComponent(FirstPersonCameraComponent,AttachmentRules);
	/*
	FVector location = GetActorLocation();
	AWeaponBean50* Bean50 = (AWeaponBean50*) GetWorld()->SpawnActor(Weapon50Class,&location);
	FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, true);
	Bean50->GetOwner()->AttachToComponent(Mesh1P,AttachmentRules);
	*/
}

void AA3_HenshawOliverCharacter::Weapon2()
{
	//Submachine Bean
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("equipped Submachine Bean"));
}

void AA3_HenshawOliverCharacter::Weapon3()
{
	//Bean Lobber
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("equipped lobber"));
}

void AA3_HenshawOliverCharacter::Ammo1()
{
	//Normal Beans
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("equipped Normal Beans"));
}

void AA3_HenshawOliverCharacter::Ammo2()
{
	//Baked Beans
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("equipped baked Beans"));
}

void AA3_HenshawOliverCharacter::Ammo3()
{
	//Frozen Beans
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("equipped Frozen Beans"));
}

void AA3_HenshawOliverCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * TurnRateGamepad * GetWorld()->GetDeltaSeconds());
}

void AA3_HenshawOliverCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * TurnRateGamepad * GetWorld()->GetDeltaSeconds());
}

bool AA3_HenshawOliverCharacter::EnableTouchscreenMovement(class UInputComponent* PlayerInputComponent)
{
	if (FPlatformMisc::SupportsTouchInput() || GetDefault<UInputSettings>()->bUseMouseForTouch)
	{
		PlayerInputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AA3_HenshawOliverCharacter::BeginTouch);
		PlayerInputComponent->BindTouch(EInputEvent::IE_Released, this, &AA3_HenshawOliverCharacter::EndTouch);

		return true;
	}
	
	return false;
}
