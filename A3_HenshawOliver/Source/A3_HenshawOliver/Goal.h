// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "GameFramework/Actor.h"
#include "Goal.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE
(FOnFinish);

UCLASS()
class A3_HENSHAWOLIVER_API AGoal : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGoal();
	UPROPERTY(BlueprintAssignable)
	FOnFinish FinishHitBoxDelegate;
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* EndMesh;
	UPROPERTY(EditAnywhere)
	UBoxComponent* FinishHitBox;
	UFUNCTION()void OnHitboxOverlapBegin(UPrimitiveComponent* OverlappedComponent,AActor* OtherActor,
 UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex,
 bool bFromSweep, const FHitResult& SweepResult);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
