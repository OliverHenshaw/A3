// Fill out your copyright notice in the Description page of Project Settings.


#include "AmmoType.h"

// Sets default values for this component's properties
UAmmoType::UAmmoType()
{
	
}


UAmmoType::UAmmoType(int damMultiplier, int slowDown, int DamageTime, int speed)
{
	
	DamageMultiplier = damMultiplier;
	Slowdown = slowDown;
	DamageOverTime = DamageTime;
	SpeedMultiplier = speed;
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	//PrimaryComponentTick.bCanEverTick = true;

	// ...
}



// Called when the game starts
void UAmmoType::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame

