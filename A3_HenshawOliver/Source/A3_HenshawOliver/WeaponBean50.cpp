// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponBean50.h"

#include "A3_HenshawOliverCharacter.h"

AWeaponBean50::AWeaponBean50()
{
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	Body = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Body"));
	Body->SetupAttachment(RootComponent);
	Barrel = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Barrel"));
	Barrel->SetupAttachment(Body);
	Sight = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Sight"));
	Sight->SetupAttachment(Body);
	Can = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Can"));
	Can->SetupAttachment(Body);

	MaxAmmo = 5;
	ProjectileSpeed  = 9400;
	CurrentAmmo = 5;
	FireRate = 0.7;
	ReloadTime = 2;
	Magnification = 4;
	BaseDamage = 170;
	
	
}

void AWeaponBean50::BeginPlay()
{
	Super::BeginPlay();
}

void AWeaponBean50::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
void Fire()
{
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Some debug message!"));	
	/*
	if (ProjectileClass != nullptr)
	{
		UWorld* const World = GetWorld();
		if (World != nullptr)
		{
			APlayerController* PlayerController = Cast<APlayerController>(Character->GetController());
			const FRotator SpawnRotation = PlayerController->PlayerCameraManager->GetCameraRotation();
			// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
			const FVector SpawnLocation = GetOwner()->GetActorLocation() + SpawnRotation.RotateVector(MuzzleOffset);
	
			//Set Spawn Collision Handling Override
			FActorSpawnParameters ActorSpawnParams;
			ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
	
			// Spawn the projectile at the muzzle
			World->SpawnActor<AA3_HenshawOliverProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);
		}
	}
	*/
	//Super::Fire();
	
	//Spawn projectile
	//give it its damage
}
