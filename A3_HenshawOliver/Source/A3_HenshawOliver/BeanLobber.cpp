// Fill out your copyright notice in the Description page of Project Settings.


#include "BeanLobber.h"

ABeanLobber::ABeanLobber()
{
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	Body = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Body"));
	Body->SetupAttachment(RootComponent);
	Spoon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Spoon"));
	Spoon->SetupAttachment(Body);
	Holder = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Holder"));
	Holder->SetupAttachment(Body);
	Can = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Can"));
	Can->SetupAttachment(Body);

	MaxAmmo = 2;
	ProjectileSpeed  = 200;
	CurrentAmmo = 2;
	FireRate = 1.5;
	ReloadTime = 2;
	Magnification = 1.1;
	BaseDamage = 280;
	
	
}

void ABeanLobber::BeginPlay()
{
	Super::BeginPlay();
}

void ABeanLobber::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}