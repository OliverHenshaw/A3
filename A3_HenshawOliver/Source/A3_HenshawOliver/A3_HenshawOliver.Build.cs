// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class A3_HenshawOliver : ModuleRules
{
	public A3_HenshawOliver(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
