// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapons.h"
#include "Bean50Weapon.generated.h"

/**
 * 
 */
UCLASS()
class A3_HENSHAWOLIVER_API UBean50Weapon : public UWeapons
{
	GENERATED_BODY()
public:
	virtual void BeginPlay() override;
	
};
