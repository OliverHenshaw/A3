// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "A3_HenshawOliver/Bean50.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBean50() {}
// Cross Module References
	A3_HENSHAWOLIVER_API UClass* Z_Construct_UClass_UBean50_NoRegister();
	A3_HENSHAWOLIVER_API UClass* Z_Construct_UClass_UBean50();
	A3_HENSHAWOLIVER_API UClass* Z_Construct_UClass_UWeapons();
	UPackage* Z_Construct_UPackage__Script_A3_HenshawOliver();
// End Cross Module References
	void UBean50::StaticRegisterNativesUBean50()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UBean50);
	UClass* Z_Construct_UClass_UBean50_NoRegister()
	{
		return UBean50::StaticClass();
	}
	struct Z_Construct_UClass_UBean50_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBean50_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UWeapons,
		(UObject* (*)())Z_Construct_UPackage__Script_A3_HenshawOliver,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBean50_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "Bean50.h" },
		{ "ModuleRelativePath", "Bean50.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBean50_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBean50>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UBean50_Statics::ClassParams = {
		&UBean50::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UBean50_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBean50_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBean50()
	{
		if (!Z_Registration_Info_UClass_UBean50.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UBean50.OuterSingleton, Z_Construct_UClass_UBean50_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UBean50.OuterSingleton;
	}
	template<> A3_HENSHAWOLIVER_API UClass* StaticClass<UBean50>()
	{
		return UBean50::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBean50);
	struct Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_Bean50_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_Bean50_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UBean50, UBean50::StaticClass, TEXT("UBean50"), &Z_Registration_Info_UClass_UBean50, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UBean50), 4003519193U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_Bean50_h_2529008255(TEXT("/Script/A3_HenshawOliver"),
		Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_Bean50_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_Bean50_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
