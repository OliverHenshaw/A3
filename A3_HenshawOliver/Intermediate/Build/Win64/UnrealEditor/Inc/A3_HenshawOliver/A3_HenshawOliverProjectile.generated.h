// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef A3_HENSHAWOLIVER_A3_HenshawOliverProjectile_generated_h
#error "A3_HenshawOliverProjectile.generated.h already included, missing '#pragma once' in A3_HenshawOliverProjectile.h"
#endif
#define A3_HENSHAWOLIVER_A3_HenshawOliverProjectile_generated_h

#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverProjectile_h_15_SPARSE_DATA
#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverProjectile_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit);


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverProjectile_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit);


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverProjectile_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAA3_HenshawOliverProjectile(); \
	friend struct Z_Construct_UClass_AA3_HenshawOliverProjectile_Statics; \
public: \
	DECLARE_CLASS(AA3_HenshawOliverProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/A3_HenshawOliver"), NO_API) \
	DECLARE_SERIALIZER(AA3_HenshawOliverProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverProjectile_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAA3_HenshawOliverProjectile(); \
	friend struct Z_Construct_UClass_AA3_HenshawOliverProjectile_Statics; \
public: \
	DECLARE_CLASS(AA3_HenshawOliverProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/A3_HenshawOliver"), NO_API) \
	DECLARE_SERIALIZER(AA3_HenshawOliverProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverProjectile_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AA3_HenshawOliverProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AA3_HenshawOliverProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AA3_HenshawOliverProjectile); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AA3_HenshawOliverProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AA3_HenshawOliverProjectile(AA3_HenshawOliverProjectile&&); \
	NO_API AA3_HenshawOliverProjectile(const AA3_HenshawOliverProjectile&); \
public:


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverProjectile_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AA3_HenshawOliverProjectile(AA3_HenshawOliverProjectile&&); \
	NO_API AA3_HenshawOliverProjectile(const AA3_HenshawOliverProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AA3_HenshawOliverProjectile); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AA3_HenshawOliverProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AA3_HenshawOliverProjectile)


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverProjectile_h_12_PROLOG
#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverProjectile_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverProjectile_h_15_SPARSE_DATA \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverProjectile_h_15_RPC_WRAPPERS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverProjectile_h_15_INCLASS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverProjectile_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverProjectile_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverProjectile_h_15_SPARSE_DATA \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverProjectile_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverProjectile_h_15_INCLASS_NO_PURE_DECLS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverProjectile_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> A3_HENSHAWOLIVER_API UClass* StaticClass<class AA3_HenshawOliverProjectile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
