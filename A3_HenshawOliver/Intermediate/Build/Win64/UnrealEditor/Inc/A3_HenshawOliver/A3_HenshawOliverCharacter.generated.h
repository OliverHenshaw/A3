// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef A3_HENSHAWOLIVER_A3_HenshawOliverCharacter_generated_h
#error "A3_HenshawOliverCharacter.generated.h already included, missing '#pragma once' in A3_HenshawOliverCharacter.h"
#endif
#define A3_HENSHAWOLIVER_A3_HenshawOliverCharacter_generated_h

#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverCharacter_h_19_DELEGATE \
static inline void FOnUseItem_DelegateWrapper(const FMulticastScriptDelegate& OnUseItem) \
{ \
	OnUseItem.ProcessMulticastDelegate<UObject>(NULL); \
}


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverCharacter_h_24_SPARSE_DATA
#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverCharacter_h_24_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetArmour); \
	DECLARE_FUNCTION(execGetHealth); \
	DECLARE_FUNCTION(execSetArmour); \
	DECLARE_FUNCTION(execGetAmmo);


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverCharacter_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetArmour); \
	DECLARE_FUNCTION(execGetHealth); \
	DECLARE_FUNCTION(execSetArmour); \
	DECLARE_FUNCTION(execGetAmmo);


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverCharacter_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAA3_HenshawOliverCharacter(); \
	friend struct Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics; \
public: \
	DECLARE_CLASS(AA3_HenshawOliverCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/A3_HenshawOliver"), NO_API) \
	DECLARE_SERIALIZER(AA3_HenshawOliverCharacter)


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverCharacter_h_24_INCLASS \
private: \
	static void StaticRegisterNativesAA3_HenshawOliverCharacter(); \
	friend struct Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics; \
public: \
	DECLARE_CLASS(AA3_HenshawOliverCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/A3_HenshawOliver"), NO_API) \
	DECLARE_SERIALIZER(AA3_HenshawOliverCharacter)


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverCharacter_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AA3_HenshawOliverCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AA3_HenshawOliverCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AA3_HenshawOliverCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AA3_HenshawOliverCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AA3_HenshawOliverCharacter(AA3_HenshawOliverCharacter&&); \
	NO_API AA3_HenshawOliverCharacter(const AA3_HenshawOliverCharacter&); \
public:


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverCharacter_h_24_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AA3_HenshawOliverCharacter(AA3_HenshawOliverCharacter&&); \
	NO_API AA3_HenshawOliverCharacter(const AA3_HenshawOliverCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AA3_HenshawOliverCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AA3_HenshawOliverCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AA3_HenshawOliverCharacter)


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverCharacter_h_21_PROLOG
#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverCharacter_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverCharacter_h_24_SPARSE_DATA \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverCharacter_h_24_RPC_WRAPPERS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverCharacter_h_24_INCLASS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverCharacter_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverCharacter_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverCharacter_h_24_SPARSE_DATA \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverCharacter_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverCharacter_h_24_INCLASS_NO_PURE_DECLS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverCharacter_h_24_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> A3_HENSHAWOLIVER_API UClass* StaticClass<class AA3_HenshawOliverCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
