// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef A3_HENSHAWOLIVER_AmmoPickup_generated_h
#error "AmmoPickup.generated.h already included, missing '#pragma once' in AmmoPickup.h"
#endif
#define A3_HENSHAWOLIVER_AmmoPickup_generated_h

#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoPickup_h_11_DELEGATE \
static inline void FOnAmmoPickup_DelegateWrapper(const FMulticastScriptDelegate& OnAmmoPickup) \
{ \
	OnAmmoPickup.ProcessMulticastDelegate<UObject>(NULL); \
}


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoPickup_h_15_SPARSE_DATA
#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoPickup_h_15_RPC_WRAPPERS
#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoPickup_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoPickup_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAmmoPickup(); \
	friend struct Z_Construct_UClass_AAmmoPickup_Statics; \
public: \
	DECLARE_CLASS(AAmmoPickup, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/A3_HenshawOliver"), NO_API) \
	DECLARE_SERIALIZER(AAmmoPickup)


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoPickup_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAAmmoPickup(); \
	friend struct Z_Construct_UClass_AAmmoPickup_Statics; \
public: \
	DECLARE_CLASS(AAmmoPickup, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/A3_HenshawOliver"), NO_API) \
	DECLARE_SERIALIZER(AAmmoPickup)


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoPickup_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAmmoPickup(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAmmoPickup) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAmmoPickup); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAmmoPickup); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAmmoPickup(AAmmoPickup&&); \
	NO_API AAmmoPickup(const AAmmoPickup&); \
public:


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoPickup_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAmmoPickup(AAmmoPickup&&); \
	NO_API AAmmoPickup(const AAmmoPickup&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAmmoPickup); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAmmoPickup); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AAmmoPickup)


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoPickup_h_12_PROLOG
#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoPickup_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoPickup_h_15_SPARSE_DATA \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoPickup_h_15_RPC_WRAPPERS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoPickup_h_15_INCLASS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoPickup_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoPickup_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoPickup_h_15_SPARSE_DATA \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoPickup_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoPickup_h_15_INCLASS_NO_PURE_DECLS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoPickup_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> A3_HENSHAWOLIVER_API UClass* StaticClass<class AAmmoPickup>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoPickup_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
