// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef A3_HENSHAWOLIVER_WeaponBean50_generated_h
#error "WeaponBean50.generated.h already included, missing '#pragma once' in WeaponBean50.h"
#endif
#define A3_HENSHAWOLIVER_WeaponBean50_generated_h

#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_WeaponBean50_h_17_SPARSE_DATA
#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_WeaponBean50_h_17_RPC_WRAPPERS
#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_WeaponBean50_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_WeaponBean50_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAWeaponBean50(); \
	friend struct Z_Construct_UClass_AWeaponBean50_Statics; \
public: \
	DECLARE_CLASS(AWeaponBean50, AWeapon, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/A3_HenshawOliver"), NO_API) \
	DECLARE_SERIALIZER(AWeaponBean50)


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_WeaponBean50_h_17_INCLASS \
private: \
	static void StaticRegisterNativesAWeaponBean50(); \
	friend struct Z_Construct_UClass_AWeaponBean50_Statics; \
public: \
	DECLARE_CLASS(AWeaponBean50, AWeapon, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/A3_HenshawOliver"), NO_API) \
	DECLARE_SERIALIZER(AWeaponBean50)


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_WeaponBean50_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AWeaponBean50(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AWeaponBean50) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWeaponBean50); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWeaponBean50); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWeaponBean50(AWeaponBean50&&); \
	NO_API AWeaponBean50(const AWeaponBean50&); \
public:


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_WeaponBean50_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWeaponBean50(AWeaponBean50&&); \
	NO_API AWeaponBean50(const AWeaponBean50&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWeaponBean50); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWeaponBean50); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AWeaponBean50)


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_WeaponBean50_h_12_PROLOG
#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_WeaponBean50_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_WeaponBean50_h_17_SPARSE_DATA \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_WeaponBean50_h_17_RPC_WRAPPERS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_WeaponBean50_h_17_INCLASS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_WeaponBean50_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_WeaponBean50_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_WeaponBean50_h_17_SPARSE_DATA \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_WeaponBean50_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_WeaponBean50_h_17_INCLASS_NO_PURE_DECLS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_WeaponBean50_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> A3_HENSHAWOLIVER_API UClass* StaticClass<class AWeaponBean50>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_A3_HenshawOliver_Source_A3_HenshawOliver_WeaponBean50_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
