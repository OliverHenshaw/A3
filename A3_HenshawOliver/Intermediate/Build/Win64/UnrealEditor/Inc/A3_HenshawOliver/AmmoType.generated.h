// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef A3_HENSHAWOLIVER_AmmoType_generated_h
#error "AmmoType.generated.h already included, missing '#pragma once' in AmmoType.h"
#endif
#define A3_HENSHAWOLIVER_AmmoType_generated_h

#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoType_h_13_SPARSE_DATA
#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoType_h_13_RPC_WRAPPERS
#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoType_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoType_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAmmoType(); \
	friend struct Z_Construct_UClass_UAmmoType_Statics; \
public: \
	DECLARE_CLASS(UAmmoType, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/A3_HenshawOliver"), NO_API) \
	DECLARE_SERIALIZER(UAmmoType)


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoType_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUAmmoType(); \
	friend struct Z_Construct_UClass_UAmmoType_Statics; \
public: \
	DECLARE_CLASS(UAmmoType, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/A3_HenshawOliver"), NO_API) \
	DECLARE_SERIALIZER(UAmmoType)


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoType_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAmmoType(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAmmoType) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAmmoType); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAmmoType); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAmmoType(UAmmoType&&); \
	NO_API UAmmoType(const UAmmoType&); \
public:


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoType_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAmmoType(UAmmoType&&); \
	NO_API UAmmoType(const UAmmoType&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAmmoType); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAmmoType); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UAmmoType)


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoType_h_10_PROLOG
#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoType_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoType_h_13_SPARSE_DATA \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoType_h_13_RPC_WRAPPERS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoType_h_13_INCLASS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoType_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoType_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoType_h_13_SPARSE_DATA \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoType_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoType_h_13_INCLASS_NO_PURE_DECLS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoType_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> A3_HENSHAWOLIVER_API UClass* StaticClass<class UAmmoType>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoType_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
