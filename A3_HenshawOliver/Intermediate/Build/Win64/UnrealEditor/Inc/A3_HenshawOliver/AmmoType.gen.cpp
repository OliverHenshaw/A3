// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "A3_HenshawOliver/AmmoType.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAmmoType() {}
// Cross Module References
	A3_HENSHAWOLIVER_API UClass* Z_Construct_UClass_UAmmoType_NoRegister();
	A3_HENSHAWOLIVER_API UClass* Z_Construct_UClass_UAmmoType();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_A3_HenshawOliver();
// End Cross Module References
	void UAmmoType::StaticRegisterNativesUAmmoType()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UAmmoType);
	UClass* Z_Construct_UClass_UAmmoType_NoRegister()
	{
		return UAmmoType::StaticClass();
	}
	struct Z_Construct_UClass_UAmmoType_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_DamageMultiplier_MetaData[];
#endif
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_DamageMultiplier;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Slowdown_MetaData[];
#endif
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_Slowdown;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_DamageOverTime_MetaData[];
#endif
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_DamageOverTime;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_SpeedMultiplier_MetaData[];
#endif
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_SpeedMultiplier;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAmmoType_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_A3_HenshawOliver,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAmmoType_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "AmmoType.h" },
		{ "ModuleRelativePath", "AmmoType.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAmmoType_Statics::NewProp_DamageMultiplier_MetaData[] = {
		{ "Category", "AmmoType" },
		{ "ModuleRelativePath", "AmmoType.h" },
	};
#endif
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UAmmoType_Statics::NewProp_DamageMultiplier = { "DamageMultiplier", nullptr, (EPropertyFlags)0x0010000000000001, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAmmoType, DamageMultiplier), METADATA_PARAMS(Z_Construct_UClass_UAmmoType_Statics::NewProp_DamageMultiplier_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAmmoType_Statics::NewProp_DamageMultiplier_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAmmoType_Statics::NewProp_Slowdown_MetaData[] = {
		{ "Category", "AmmoType" },
		{ "ModuleRelativePath", "AmmoType.h" },
	};
#endif
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UAmmoType_Statics::NewProp_Slowdown = { "Slowdown", nullptr, (EPropertyFlags)0x0010000000000001, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAmmoType, Slowdown), METADATA_PARAMS(Z_Construct_UClass_UAmmoType_Statics::NewProp_Slowdown_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAmmoType_Statics::NewProp_Slowdown_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAmmoType_Statics::NewProp_DamageOverTime_MetaData[] = {
		{ "Category", "AmmoType" },
		{ "ModuleRelativePath", "AmmoType.h" },
	};
#endif
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UAmmoType_Statics::NewProp_DamageOverTime = { "DamageOverTime", nullptr, (EPropertyFlags)0x0010000000000001, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAmmoType, DamageOverTime), METADATA_PARAMS(Z_Construct_UClass_UAmmoType_Statics::NewProp_DamageOverTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAmmoType_Statics::NewProp_DamageOverTime_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAmmoType_Statics::NewProp_SpeedMultiplier_MetaData[] = {
		{ "Category", "AmmoType" },
		{ "ModuleRelativePath", "AmmoType.h" },
	};
#endif
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UAmmoType_Statics::NewProp_SpeedMultiplier = { "SpeedMultiplier", nullptr, (EPropertyFlags)0x0010000000000001, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAmmoType, SpeedMultiplier), METADATA_PARAMS(Z_Construct_UClass_UAmmoType_Statics::NewProp_SpeedMultiplier_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAmmoType_Statics::NewProp_SpeedMultiplier_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAmmoType_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAmmoType_Statics::NewProp_DamageMultiplier,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAmmoType_Statics::NewProp_Slowdown,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAmmoType_Statics::NewProp_DamageOverTime,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAmmoType_Statics::NewProp_SpeedMultiplier,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAmmoType_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAmmoType>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UAmmoType_Statics::ClassParams = {
		&UAmmoType::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAmmoType_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UAmmoType_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UAmmoType_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAmmoType_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAmmoType()
	{
		if (!Z_Registration_Info_UClass_UAmmoType.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UAmmoType.OuterSingleton, Z_Construct_UClass_UAmmoType_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UAmmoType.OuterSingleton;
	}
	template<> A3_HENSHAWOLIVER_API UClass* StaticClass<UAmmoType>()
	{
		return UAmmoType::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAmmoType);
	struct Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoType_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoType_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UAmmoType, UAmmoType::StaticClass, TEXT("UAmmoType"), &Z_Registration_Info_UClass_UAmmoType, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UAmmoType), 3526615545U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoType_h_2685628111(TEXT("/Script/A3_HenshawOliver"),
		Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoType_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_AmmoType_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
