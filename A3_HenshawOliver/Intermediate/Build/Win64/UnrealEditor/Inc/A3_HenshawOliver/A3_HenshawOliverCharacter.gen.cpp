// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "A3_HenshawOliver/A3_HenshawOliverCharacter.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeA3_HenshawOliverCharacter() {}
// Cross Module References
	A3_HENSHAWOLIVER_API UFunction* Z_Construct_UDelegateFunction_A3_HenshawOliver_OnUseItem__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_A3_HenshawOliver();
	A3_HENSHAWOLIVER_API UClass* Z_Construct_UClass_AA3_HenshawOliverCharacter_NoRegister();
	A3_HENSHAWOLIVER_API UClass* Z_Construct_UClass_AA3_HenshawOliverCharacter();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	ENGINE_API UClass* Z_Construct_UClass_USkeletalMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	A3_HENSHAWOLIVER_API UClass* Z_Construct_UClass_AWeaponBean50_NoRegister();
	A3_HENSHAWOLIVER_API UClass* Z_Construct_UClass_AWeapon_NoRegister();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_A3_HenshawOliver_OnUseItem__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_A3_HenshawOliver_OnUseItem__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "// Declaration of the delegate that will be called when the Primary Action is triggered\n// It is declared as dynamic so it can be accessed also in Blueprints\n" },
		{ "ModuleRelativePath", "A3_HenshawOliverCharacter.h" },
		{ "ToolTip", "Declaration of the delegate that will be called when the Primary Action is triggered\nIt is declared as dynamic so it can be accessed also in Blueprints" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_A3_HenshawOliver_OnUseItem__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_A3_HenshawOliver, nullptr, "OnUseItem__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_A3_HenshawOliver_OnUseItem__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_A3_HenshawOliver_OnUseItem__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_A3_HenshawOliver_OnUseItem__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UDelegateFunction_A3_HenshawOliver_OnUseItem__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(AA3_HenshawOliverCharacter::execGetArmour)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetArmour();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AA3_HenshawOliverCharacter::execGetHealth)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetHealth();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AA3_HenshawOliverCharacter::execSetArmour)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_newAmmo);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetArmour(Z_Param_newAmmo);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AA3_HenshawOliverCharacter::execGetAmmo)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetAmmo();
		P_NATIVE_END;
	}
	void AA3_HenshawOliverCharacter::StaticRegisterNativesAA3_HenshawOliverCharacter()
	{
		UClass* Class = AA3_HenshawOliverCharacter::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetAmmo", &AA3_HenshawOliverCharacter::execGetAmmo },
			{ "GetArmour", &AA3_HenshawOliverCharacter::execGetArmour },
			{ "GetHealth", &AA3_HenshawOliverCharacter::execGetHealth },
			{ "SetArmour", &AA3_HenshawOliverCharacter::execSetArmour },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetAmmo_Statics
	{
		struct A3_HenshawOliverCharacter_eventGetAmmo_Parms
		{
			int32 ReturnValue;
		};
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetAmmo_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(A3_HenshawOliverCharacter_eventGetAmmo_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetAmmo_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetAmmo_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetAmmo_Statics::Function_MetaDataParams[] = {
		{ "Category", "Player Stats" },
		{ "Comment", "//Get Player Stats for HUD\n" },
		{ "ModuleRelativePath", "A3_HenshawOliverCharacter.h" },
		{ "ToolTip", "Get Player Stats for HUD" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetAmmo_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AA3_HenshawOliverCharacter, nullptr, "GetAmmo", nullptr, nullptr, sizeof(Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetAmmo_Statics::A3_HenshawOliverCharacter_eventGetAmmo_Parms), Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetAmmo_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetAmmo_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetAmmo_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetAmmo_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetAmmo()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetAmmo_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetArmour_Statics
	{
		struct A3_HenshawOliverCharacter_eventGetArmour_Parms
		{
			int32 ReturnValue;
		};
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetArmour_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(A3_HenshawOliverCharacter_eventGetArmour_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetArmour_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetArmour_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetArmour_Statics::Function_MetaDataParams[] = {
		{ "Category", "Player Stats" },
		{ "ModuleRelativePath", "A3_HenshawOliverCharacter.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetArmour_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AA3_HenshawOliverCharacter, nullptr, "GetArmour", nullptr, nullptr, sizeof(Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetArmour_Statics::A3_HenshawOliverCharacter_eventGetArmour_Parms), Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetArmour_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetArmour_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetArmour_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetArmour_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetArmour()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetArmour_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetHealth_Statics
	{
		struct A3_HenshawOliverCharacter_eventGetHealth_Parms
		{
			int32 ReturnValue;
		};
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetHealth_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(A3_HenshawOliverCharacter_eventGetHealth_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetHealth_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetHealth_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetHealth_Statics::Function_MetaDataParams[] = {
		{ "Category", "Player Stats" },
		{ "ModuleRelativePath", "A3_HenshawOliverCharacter.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetHealth_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AA3_HenshawOliverCharacter, nullptr, "GetHealth", nullptr, nullptr, sizeof(Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetHealth_Statics::A3_HenshawOliverCharacter_eventGetHealth_Parms), Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetHealth_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetHealth_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetHealth_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetHealth_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetHealth()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetHealth_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AA3_HenshawOliverCharacter_SetArmour_Statics
	{
		struct A3_HenshawOliverCharacter_eventSetArmour_Parms
		{
			int32 newAmmo;
		};
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_newAmmo;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_AA3_HenshawOliverCharacter_SetArmour_Statics::NewProp_newAmmo = { "newAmmo", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(A3_HenshawOliverCharacter_eventSetArmour_Parms, newAmmo), METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AA3_HenshawOliverCharacter_SetArmour_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AA3_HenshawOliverCharacter_SetArmour_Statics::NewProp_newAmmo,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AA3_HenshawOliverCharacter_SetArmour_Statics::Function_MetaDataParams[] = {
		{ "Category", "Player Stats" },
		{ "ModuleRelativePath", "A3_HenshawOliverCharacter.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_AA3_HenshawOliverCharacter_SetArmour_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AA3_HenshawOliverCharacter, nullptr, "SetArmour", nullptr, nullptr, sizeof(Z_Construct_UFunction_AA3_HenshawOliverCharacter_SetArmour_Statics::A3_HenshawOliverCharacter_eventSetArmour_Parms), Z_Construct_UFunction_AA3_HenshawOliverCharacter_SetArmour_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AA3_HenshawOliverCharacter_SetArmour_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AA3_HenshawOliverCharacter_SetArmour_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AA3_HenshawOliverCharacter_SetArmour_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AA3_HenshawOliverCharacter_SetArmour()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_AA3_HenshawOliverCharacter_SetArmour_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(AA3_HenshawOliverCharacter);
	UClass* Z_Construct_UClass_AA3_HenshawOliverCharacter_NoRegister()
	{
		return AA3_HenshawOliverCharacter::StaticClass();
	}
	struct Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Mesh1P_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Mesh1P;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_FirstPersonCameraComponent_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_FirstPersonCameraComponent;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Weapon50Class_MetaData[];
#endif
		static const UECodeGen_Private::FClassPropertyParams NewProp_Weapon50Class;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_WeaponHolder_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_WeaponHolder;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Ammo_MetaData[];
#endif
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_Ammo;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Health_MetaData[];
#endif
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_Health;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Armour_MetaData[];
#endif
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_Armour;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_TurnRateGamepad_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_TurnRateGamepad;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_OnUseItem_MetaData[];
#endif
		static const UECodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnUseItem;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_A3_HenshawOliver,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetAmmo, "GetAmmo" }, // 3501806797
		{ &Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetArmour, "GetArmour" }, // 54270871
		{ &Z_Construct_UFunction_AA3_HenshawOliverCharacter_GetHealth, "GetHealth" }, // 4115817971
		{ &Z_Construct_UFunction_AA3_HenshawOliverCharacter_SetArmour, "SetArmour" }, // 4234441669
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "A3_HenshawOliverCharacter.h" },
		{ "ModuleRelativePath", "A3_HenshawOliverCharacter.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_Mesh1P_MetaData[] = {
		{ "Category", "Mesh" },
		{ "Comment", "/** Pawn mesh: 1st person view (arms; seen only by self) */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "A3_HenshawOliverCharacter.h" },
		{ "ToolTip", "Pawn mesh: 1st person view (arms; seen only by self)" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_Mesh1P = { "Mesh1P", nullptr, (EPropertyFlags)0x00400000000b0009, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AA3_HenshawOliverCharacter, Mesh1P), Z_Construct_UClass_USkeletalMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_Mesh1P_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_Mesh1P_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_FirstPersonCameraComponent_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "Comment", "/** First person camera */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "A3_HenshawOliverCharacter.h" },
		{ "ToolTip", "First person camera" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_FirstPersonCameraComponent = { "FirstPersonCameraComponent", nullptr, (EPropertyFlags)0x00400000000a001d, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AA3_HenshawOliverCharacter, FirstPersonCameraComponent), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_FirstPersonCameraComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_FirstPersonCameraComponent_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_Weapon50Class_MetaData[] = {
		{ "Category", "Projectile" },
		{ "Comment", "//Weapon\n" },
		{ "ModuleRelativePath", "A3_HenshawOliverCharacter.h" },
		{ "ToolTip", "Weapon" },
	};
#endif
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_Weapon50Class = { "Weapon50Class", nullptr, (EPropertyFlags)0x0044000000000001, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AA3_HenshawOliverCharacter, Weapon50Class), Z_Construct_UClass_AWeaponBean50_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_Weapon50Class_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_Weapon50Class_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_WeaponHolder_MetaData[] = {
		{ "Category", "Projectile" },
		{ "ModuleRelativePath", "A3_HenshawOliverCharacter.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_WeaponHolder = { "WeaponHolder", nullptr, (EPropertyFlags)0x0040000000000001, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AA3_HenshawOliverCharacter, WeaponHolder), Z_Construct_UClass_AWeapon_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_WeaponHolder_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_WeaponHolder_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_Ammo_MetaData[] = {
		{ "Category", "Player Stats" },
		{ "Comment", "//Player Stats\n" },
		{ "ModuleRelativePath", "A3_HenshawOliverCharacter.h" },
		{ "ToolTip", "Player Stats" },
	};
#endif
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_Ammo = { "Ammo", nullptr, (EPropertyFlags)0x0040000000000001, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AA3_HenshawOliverCharacter, Ammo), METADATA_PARAMS(Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_Ammo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_Ammo_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_Health_MetaData[] = {
		{ "Category", "Player Stats" },
		{ "ModuleRelativePath", "A3_HenshawOliverCharacter.h" },
	};
#endif
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_Health = { "Health", nullptr, (EPropertyFlags)0x0040000000000001, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AA3_HenshawOliverCharacter, Health), METADATA_PARAMS(Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_Health_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_Health_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_Armour_MetaData[] = {
		{ "Category", "Player Stats" },
		{ "ModuleRelativePath", "A3_HenshawOliverCharacter.h" },
	};
#endif
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_Armour = { "Armour", nullptr, (EPropertyFlags)0x0040000000000001, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AA3_HenshawOliverCharacter, Armour), METADATA_PARAMS(Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_Armour_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_Armour_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_TurnRateGamepad_MetaData[] = {
		{ "Category", "Camera" },
		{ "Comment", "/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */" },
		{ "ModuleRelativePath", "A3_HenshawOliverCharacter.h" },
		{ "ToolTip", "Base turn rate, in deg/sec. Other scaling may affect final turn rate." },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_TurnRateGamepad = { "TurnRateGamepad", nullptr, (EPropertyFlags)0x0010000000020015, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AA3_HenshawOliverCharacter, TurnRateGamepad), METADATA_PARAMS(Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_TurnRateGamepad_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_TurnRateGamepad_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_OnUseItem_MetaData[] = {
		{ "Category", "Interaction" },
		{ "Comment", "/** Delegate to whom anyone can subscribe to receive this event */" },
		{ "ModuleRelativePath", "A3_HenshawOliverCharacter.h" },
		{ "ToolTip", "Delegate to whom anyone can subscribe to receive this event" },
	};
#endif
	const UECodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_OnUseItem = { "OnUseItem", nullptr, (EPropertyFlags)0x0010000010080000, UECodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AA3_HenshawOliverCharacter, OnUseItem), Z_Construct_UDelegateFunction_A3_HenshawOliver_OnUseItem__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_OnUseItem_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_OnUseItem_MetaData)) }; // 731666857
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_Mesh1P,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_FirstPersonCameraComponent,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_Weapon50Class,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_WeaponHolder,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_Ammo,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_Health,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_Armour,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_TurnRateGamepad,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::NewProp_OnUseItem,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AA3_HenshawOliverCharacter>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::ClassParams = {
		&AA3_HenshawOliverCharacter::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::PropPointers),
		0,
		0x008000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AA3_HenshawOliverCharacter()
	{
		if (!Z_Registration_Info_UClass_AA3_HenshawOliverCharacter.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_AA3_HenshawOliverCharacter.OuterSingleton, Z_Construct_UClass_AA3_HenshawOliverCharacter_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_AA3_HenshawOliverCharacter.OuterSingleton;
	}
	template<> A3_HENSHAWOLIVER_API UClass* StaticClass<AA3_HenshawOliverCharacter>()
	{
		return AA3_HenshawOliverCharacter::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(AA3_HenshawOliverCharacter);
	struct Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverCharacter_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverCharacter_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_AA3_HenshawOliverCharacter, AA3_HenshawOliverCharacter::StaticClass, TEXT("AA3_HenshawOliverCharacter"), &Z_Registration_Info_UClass_AA3_HenshawOliverCharacter, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(AA3_HenshawOliverCharacter), 3607166671U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverCharacter_h_3488945633(TEXT("/Script/A3_HenshawOliver"),
		Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverCharacter_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverCharacter_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
