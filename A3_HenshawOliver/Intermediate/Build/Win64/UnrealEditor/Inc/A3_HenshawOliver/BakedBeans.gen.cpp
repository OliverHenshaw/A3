// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "A3_HenshawOliver/BakedBeans.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBakedBeans() {}
// Cross Module References
	A3_HENSHAWOLIVER_API UClass* Z_Construct_UClass_UBakedBeans_NoRegister();
	A3_HENSHAWOLIVER_API UClass* Z_Construct_UClass_UBakedBeans();
	A3_HENSHAWOLIVER_API UClass* Z_Construct_UClass_UAmmoType();
	UPackage* Z_Construct_UPackage__Script_A3_HenshawOliver();
// End Cross Module References
	void UBakedBeans::StaticRegisterNativesUBakedBeans()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UBakedBeans);
	UClass* Z_Construct_UClass_UBakedBeans_NoRegister()
	{
		return UBakedBeans::StaticClass();
	}
	struct Z_Construct_UClass_UBakedBeans_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBakedBeans_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAmmoType,
		(UObject* (*)())Z_Construct_UPackage__Script_A3_HenshawOliver,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBakedBeans_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "BakedBeans.h" },
		{ "ModuleRelativePath", "BakedBeans.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBakedBeans_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBakedBeans>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UBakedBeans_Statics::ClassParams = {
		&UBakedBeans::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UBakedBeans_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBakedBeans_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBakedBeans()
	{
		if (!Z_Registration_Info_UClass_UBakedBeans.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UBakedBeans.OuterSingleton, Z_Construct_UClass_UBakedBeans_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UBakedBeans.OuterSingleton;
	}
	template<> A3_HENSHAWOLIVER_API UClass* StaticClass<UBakedBeans>()
	{
		return UBakedBeans::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBakedBeans);
	struct Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_BakedBeans_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_BakedBeans_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UBakedBeans, UBakedBeans::StaticClass, TEXT("UBakedBeans"), &Z_Registration_Info_UClass_UBakedBeans, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UBakedBeans), 908244872U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_BakedBeans_h_1940828458(TEXT("/Script/A3_HenshawOliver"),
		Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_BakedBeans_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_BakedBeans_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
