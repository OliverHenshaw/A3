// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef A3_HENSHAWOLIVER_Goal_generated_h
#error "Goal.generated.h already included, missing '#pragma once' in Goal.h"
#endif
#define A3_HENSHAWOLIVER_Goal_generated_h

#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_Goal_h_11_DELEGATE \
static inline void FOnFinish_DelegateWrapper(const FMulticastScriptDelegate& OnFinish) \
{ \
	OnFinish.ProcessMulticastDelegate<UObject>(NULL); \
}


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_Goal_h_16_SPARSE_DATA
#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_Goal_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHitboxOverlapBegin);


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_Goal_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHitboxOverlapBegin);


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_Goal_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGoal(); \
	friend struct Z_Construct_UClass_AGoal_Statics; \
public: \
	DECLARE_CLASS(AGoal, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/A3_HenshawOliver"), NO_API) \
	DECLARE_SERIALIZER(AGoal)


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_Goal_h_16_INCLASS \
private: \
	static void StaticRegisterNativesAGoal(); \
	friend struct Z_Construct_UClass_AGoal_Statics; \
public: \
	DECLARE_CLASS(AGoal, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/A3_HenshawOliver"), NO_API) \
	DECLARE_SERIALIZER(AGoal)


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_Goal_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGoal(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGoal) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGoal); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGoal); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGoal(AGoal&&); \
	NO_API AGoal(const AGoal&); \
public:


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_Goal_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGoal(AGoal&&); \
	NO_API AGoal(const AGoal&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGoal); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGoal); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AGoal)


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_Goal_h_13_PROLOG
#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_Goal_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_Goal_h_16_SPARSE_DATA \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_Goal_h_16_RPC_WRAPPERS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_Goal_h_16_INCLASS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_Goal_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_Goal_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_Goal_h_16_SPARSE_DATA \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_Goal_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_Goal_h_16_INCLASS_NO_PURE_DECLS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_Goal_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> A3_HENSHAWOLIVER_API UClass* StaticClass<class AGoal>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_A3_HenshawOliver_Source_A3_HenshawOliver_Goal_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
