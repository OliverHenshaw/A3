// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "A3_HenshawOliver/Weapons.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWeapons() {}
// Cross Module References
	A3_HENSHAWOLIVER_API UClass* Z_Construct_UClass_UWeapons_NoRegister();
	A3_HENSHAWOLIVER_API UClass* Z_Construct_UClass_UWeapons();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_A3_HenshawOliver();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	A3_HENSHAWOLIVER_API UClass* Z_Construct_UClass_UAmmoType_NoRegister();
// End Cross Module References
	void UWeapons::StaticRegisterNativesUWeapons()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UWeapons);
	UClass* Z_Construct_UClass_UWeapons_NoRegister()
	{
		return UWeapons::StaticClass();
	}
	struct Z_Construct_UClass_UWeapons_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_MaxAmmo_MetaData[];
#endif
		static const UECodeGen_Private::FUnsizedIntPropertyParams NewProp_MaxAmmo;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_AmmoClass_MetaData[];
#endif
		static const UECodeGen_Private::FClassPropertyParams NewProp_AmmoClass;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWeapons_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_A3_HenshawOliver,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWeapons_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Custom" },
		{ "IncludePath", "Weapons.h" },
		{ "ModuleRelativePath", "Weapons.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWeapons_Statics::NewProp_MaxAmmo_MetaData[] = {
		{ "Category", "Weapons" },
		{ "ModuleRelativePath", "Weapons.h" },
	};
#endif
	const UECodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UWeapons_Statics::NewProp_MaxAmmo = { "MaxAmmo", nullptr, (EPropertyFlags)0x0040000000000001, UECodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWeapons, MaxAmmo), METADATA_PARAMS(Z_Construct_UClass_UWeapons_Statics::NewProp_MaxAmmo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWeapons_Statics::NewProp_MaxAmmo_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWeapons_Statics::NewProp_AmmoClass_MetaData[] = {
		{ "Category", "Ammo" },
		{ "ModuleRelativePath", "Weapons.h" },
	};
#endif
	const UECodeGen_Private::FClassPropertyParams Z_Construct_UClass_UWeapons_Statics::NewProp_AmmoClass = { "AmmoClass", nullptr, (EPropertyFlags)0x0044000000000001, UECodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWeapons, AmmoClass), Z_Construct_UClass_UAmmoType_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UWeapons_Statics::NewProp_AmmoClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWeapons_Statics::NewProp_AmmoClass_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UWeapons_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWeapons_Statics::NewProp_MaxAmmo,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWeapons_Statics::NewProp_AmmoClass,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWeapons_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWeapons>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UWeapons_Statics::ClassParams = {
		&UWeapons::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UWeapons_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UWeapons_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UWeapons_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWeapons_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWeapons()
	{
		if (!Z_Registration_Info_UClass_UWeapons.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UWeapons.OuterSingleton, Z_Construct_UClass_UWeapons_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UWeapons.OuterSingleton;
	}
	template<> A3_HENSHAWOLIVER_API UClass* StaticClass<UWeapons>()
	{
		return UWeapons::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWeapons);
	struct Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_Weapons_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_Weapons_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UWeapons, UWeapons::StaticClass, TEXT("UWeapons"), &Z_Registration_Info_UClass_UWeapons, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UWeapons), 470812807U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_Weapons_h_2416705990(TEXT("/Script/A3_HenshawOliver"),
		Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_Weapons_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_Weapons_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
