// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef A3_HENSHAWOLIVER_SubmachineBean_generated_h
#error "SubmachineBean.generated.h already included, missing '#pragma once' in SubmachineBean.h"
#endif
#define A3_HENSHAWOLIVER_SubmachineBean_generated_h

#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_SubmachineBean_h_16_SPARSE_DATA
#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_SubmachineBean_h_16_RPC_WRAPPERS
#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_SubmachineBean_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_SubmachineBean_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASubmachineBean(); \
	friend struct Z_Construct_UClass_ASubmachineBean_Statics; \
public: \
	DECLARE_CLASS(ASubmachineBean, AWeapon, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/A3_HenshawOliver"), NO_API) \
	DECLARE_SERIALIZER(ASubmachineBean)


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_SubmachineBean_h_16_INCLASS \
private: \
	static void StaticRegisterNativesASubmachineBean(); \
	friend struct Z_Construct_UClass_ASubmachineBean_Statics; \
public: \
	DECLARE_CLASS(ASubmachineBean, AWeapon, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/A3_HenshawOliver"), NO_API) \
	DECLARE_SERIALIZER(ASubmachineBean)


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_SubmachineBean_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASubmachineBean(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASubmachineBean) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASubmachineBean); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASubmachineBean); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASubmachineBean(ASubmachineBean&&); \
	NO_API ASubmachineBean(const ASubmachineBean&); \
public:


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_SubmachineBean_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASubmachineBean(ASubmachineBean&&); \
	NO_API ASubmachineBean(const ASubmachineBean&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASubmachineBean); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASubmachineBean); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASubmachineBean)


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_SubmachineBean_h_12_PROLOG
#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_SubmachineBean_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_SubmachineBean_h_16_SPARSE_DATA \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_SubmachineBean_h_16_RPC_WRAPPERS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_SubmachineBean_h_16_INCLASS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_SubmachineBean_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_SubmachineBean_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_SubmachineBean_h_16_SPARSE_DATA \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_SubmachineBean_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_SubmachineBean_h_16_INCLASS_NO_PURE_DECLS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_SubmachineBean_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> A3_HENSHAWOLIVER_API UClass* StaticClass<class ASubmachineBean>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_A3_HenshawOliver_Source_A3_HenshawOliver_SubmachineBean_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
