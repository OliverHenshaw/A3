// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeA3_HenshawOliver_init() {}
	A3_HENSHAWOLIVER_API UFunction* Z_Construct_UDelegateFunction_A3_HenshawOliver_OnAmmoPickup__DelegateSignature();
	A3_HENSHAWOLIVER_API UFunction* Z_Construct_UDelegateFunction_A3_HenshawOliver_OnFinish__DelegateSignature();
	A3_HENSHAWOLIVER_API UFunction* Z_Construct_UDelegateFunction_A3_HenshawOliver_OnPickUp__DelegateSignature();
	A3_HENSHAWOLIVER_API UFunction* Z_Construct_UDelegateFunction_A3_HenshawOliver_OnUseItem__DelegateSignature();
	static FPackageRegistrationInfo Z_Registration_Info_UPackage__Script_A3_HenshawOliver;
	FORCENOINLINE UPackage* Z_Construct_UPackage__Script_A3_HenshawOliver()
	{
		if (!Z_Registration_Info_UPackage__Script_A3_HenshawOliver.OuterSingleton)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_A3_HenshawOliver_OnAmmoPickup__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_A3_HenshawOliver_OnFinish__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_A3_HenshawOliver_OnPickUp__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_A3_HenshawOliver_OnUseItem__DelegateSignature,
			};
			static const UECodeGen_Private::FPackageParams PackageParams = {
				"/Script/A3_HenshawOliver",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0x41AE0C8E,
				0x48EA0645,
				METADATA_PARAMS(nullptr, 0)
			};
			UECodeGen_Private::ConstructUPackage(Z_Registration_Info_UPackage__Script_A3_HenshawOliver.OuterSingleton, PackageParams);
		}
		return Z_Registration_Info_UPackage__Script_A3_HenshawOliver.OuterSingleton;
	}
	static FRegisterCompiledInInfo Z_CompiledInDeferPackage_UPackage__Script_A3_HenshawOliver(Z_Construct_UPackage__Script_A3_HenshawOliver, TEXT("/Script/A3_HenshawOliver"), Z_Registration_Info_UPackage__Script_A3_HenshawOliver, CONSTRUCT_RELOAD_VERSION_INFO(FPackageReloadVersionInfo, 0x41AE0C8E, 0x48EA0645));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
