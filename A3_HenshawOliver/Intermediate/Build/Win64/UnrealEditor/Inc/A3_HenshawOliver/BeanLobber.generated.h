// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef A3_HENSHAWOLIVER_BeanLobber_generated_h
#error "BeanLobber.generated.h already included, missing '#pragma once' in BeanLobber.h"
#endif
#define A3_HENSHAWOLIVER_BeanLobber_generated_h

#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_BeanLobber_h_15_SPARSE_DATA
#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_BeanLobber_h_15_RPC_WRAPPERS
#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_BeanLobber_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_BeanLobber_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABeanLobber(); \
	friend struct Z_Construct_UClass_ABeanLobber_Statics; \
public: \
	DECLARE_CLASS(ABeanLobber, AWeapon, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/A3_HenshawOliver"), NO_API) \
	DECLARE_SERIALIZER(ABeanLobber)


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_BeanLobber_h_15_INCLASS \
private: \
	static void StaticRegisterNativesABeanLobber(); \
	friend struct Z_Construct_UClass_ABeanLobber_Statics; \
public: \
	DECLARE_CLASS(ABeanLobber, AWeapon, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/A3_HenshawOliver"), NO_API) \
	DECLARE_SERIALIZER(ABeanLobber)


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_BeanLobber_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABeanLobber(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABeanLobber) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABeanLobber); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABeanLobber); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABeanLobber(ABeanLobber&&); \
	NO_API ABeanLobber(const ABeanLobber&); \
public:


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_BeanLobber_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABeanLobber(ABeanLobber&&); \
	NO_API ABeanLobber(const ABeanLobber&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABeanLobber); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABeanLobber); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABeanLobber)


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_BeanLobber_h_12_PROLOG
#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_BeanLobber_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_BeanLobber_h_15_SPARSE_DATA \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_BeanLobber_h_15_RPC_WRAPPERS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_BeanLobber_h_15_INCLASS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_BeanLobber_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_BeanLobber_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_BeanLobber_h_15_SPARSE_DATA \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_BeanLobber_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_BeanLobber_h_15_INCLASS_NO_PURE_DECLS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_BeanLobber_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> A3_HENSHAWOLIVER_API UClass* StaticClass<class ABeanLobber>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_A3_HenshawOliver_Source_A3_HenshawOliver_BeanLobber_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
