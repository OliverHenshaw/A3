// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "A3_HenshawOliver/Bean50Weapon.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBean50Weapon() {}
// Cross Module References
	A3_HENSHAWOLIVER_API UClass* Z_Construct_UClass_UBean50Weapon_NoRegister();
	A3_HENSHAWOLIVER_API UClass* Z_Construct_UClass_UBean50Weapon();
	A3_HENSHAWOLIVER_API UClass* Z_Construct_UClass_UWeapons();
	UPackage* Z_Construct_UPackage__Script_A3_HenshawOliver();
// End Cross Module References
	void UBean50Weapon::StaticRegisterNativesUBean50Weapon()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UBean50Weapon);
	UClass* Z_Construct_UClass_UBean50Weapon_NoRegister()
	{
		return UBean50Weapon::StaticClass();
	}
	struct Z_Construct_UClass_UBean50Weapon_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBean50Weapon_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UWeapons,
		(UObject* (*)())Z_Construct_UPackage__Script_A3_HenshawOliver,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBean50Weapon_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "Bean50Weapon.h" },
		{ "ModuleRelativePath", "Bean50Weapon.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBean50Weapon_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBean50Weapon>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UBean50Weapon_Statics::ClassParams = {
		&UBean50Weapon::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UBean50Weapon_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBean50Weapon_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBean50Weapon()
	{
		if (!Z_Registration_Info_UClass_UBean50Weapon.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UBean50Weapon.OuterSingleton, Z_Construct_UClass_UBean50Weapon_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UBean50Weapon.OuterSingleton;
	}
	template<> A3_HENSHAWOLIVER_API UClass* StaticClass<UBean50Weapon>()
	{
		return UBean50Weapon::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBean50Weapon);
	struct Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_Bean50Weapon_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_Bean50Weapon_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UBean50Weapon, UBean50Weapon::StaticClass, TEXT("UBean50Weapon"), &Z_Registration_Info_UClass_UBean50Weapon, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UBean50Weapon), 2298582781U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_Bean50Weapon_h_3038973261(TEXT("/Script/A3_HenshawOliver"),
		Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_Bean50Weapon_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_Bean50Weapon_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
