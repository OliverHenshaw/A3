// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "A3_HenshawOliver/A3_HenshawOliverGameMode.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeA3_HenshawOliverGameMode() {}
// Cross Module References
	A3_HENSHAWOLIVER_API UClass* Z_Construct_UClass_AA3_HenshawOliverGameMode_NoRegister();
	A3_HENSHAWOLIVER_API UClass* Z_Construct_UClass_AA3_HenshawOliverGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_A3_HenshawOliver();
// End Cross Module References
	void AA3_HenshawOliverGameMode::StaticRegisterNativesAA3_HenshawOliverGameMode()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(AA3_HenshawOliverGameMode);
	UClass* Z_Construct_UClass_AA3_HenshawOliverGameMode_NoRegister()
	{
		return AA3_HenshawOliverGameMode::StaticClass();
	}
	struct Z_Construct_UClass_AA3_HenshawOliverGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AA3_HenshawOliverGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_A3_HenshawOliver,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AA3_HenshawOliverGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering HLOD WorldPartition DataLayers Transformation" },
		{ "IncludePath", "A3_HenshawOliverGameMode.h" },
		{ "ModuleRelativePath", "A3_HenshawOliverGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AA3_HenshawOliverGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AA3_HenshawOliverGameMode>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_AA3_HenshawOliverGameMode_Statics::ClassParams = {
		&AA3_HenshawOliverGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_AA3_HenshawOliverGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AA3_HenshawOliverGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AA3_HenshawOliverGameMode()
	{
		if (!Z_Registration_Info_UClass_AA3_HenshawOliverGameMode.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_AA3_HenshawOliverGameMode.OuterSingleton, Z_Construct_UClass_AA3_HenshawOliverGameMode_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_AA3_HenshawOliverGameMode.OuterSingleton;
	}
	template<> A3_HENSHAWOLIVER_API UClass* StaticClass<AA3_HenshawOliverGameMode>()
	{
		return AA3_HenshawOliverGameMode::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(AA3_HenshawOliverGameMode);
	struct Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverGameMode_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverGameMode_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_AA3_HenshawOliverGameMode, AA3_HenshawOliverGameMode::StaticClass, TEXT("AA3_HenshawOliverGameMode"), &Z_Registration_Info_UClass_AA3_HenshawOliverGameMode, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(AA3_HenshawOliverGameMode), 3302802427U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverGameMode_h_268599479(TEXT("/Script/A3_HenshawOliver"),
		Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverGameMode_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverGameMode_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
