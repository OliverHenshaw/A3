// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "A3_HenshawOliver/FrozenBeans.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFrozenBeans() {}
// Cross Module References
	A3_HENSHAWOLIVER_API UClass* Z_Construct_UClass_UFrozenBeans_NoRegister();
	A3_HENSHAWOLIVER_API UClass* Z_Construct_UClass_UFrozenBeans();
	A3_HENSHAWOLIVER_API UClass* Z_Construct_UClass_UAmmoType();
	UPackage* Z_Construct_UPackage__Script_A3_HenshawOliver();
// End Cross Module References
	void UFrozenBeans::StaticRegisterNativesUFrozenBeans()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UFrozenBeans);
	UClass* Z_Construct_UClass_UFrozenBeans_NoRegister()
	{
		return UFrozenBeans::StaticClass();
	}
	struct Z_Construct_UClass_UFrozenBeans_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFrozenBeans_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAmmoType,
		(UObject* (*)())Z_Construct_UPackage__Script_A3_HenshawOliver,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFrozenBeans_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "FrozenBeans.h" },
		{ "ModuleRelativePath", "FrozenBeans.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFrozenBeans_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFrozenBeans>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UFrozenBeans_Statics::ClassParams = {
		&UFrozenBeans::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UFrozenBeans_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFrozenBeans_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFrozenBeans()
	{
		if (!Z_Registration_Info_UClass_UFrozenBeans.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UFrozenBeans.OuterSingleton, Z_Construct_UClass_UFrozenBeans_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UFrozenBeans.OuterSingleton;
	}
	template<> A3_HENSHAWOLIVER_API UClass* StaticClass<UFrozenBeans>()
	{
		return UFrozenBeans::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFrozenBeans);
	struct Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_FrozenBeans_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_FrozenBeans_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UFrozenBeans, UFrozenBeans::StaticClass, TEXT("UFrozenBeans"), &Z_Registration_Info_UClass_UFrozenBeans, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UFrozenBeans), 119036685U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_FrozenBeans_h_3308796057(TEXT("/Script/A3_HenshawOliver"),
		Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_FrozenBeans_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_FrozenBeans_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
