// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "A3_HenshawOliver/WeaponBean50.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWeaponBean50() {}
// Cross Module References
	A3_HENSHAWOLIVER_API UClass* Z_Construct_UClass_AWeaponBean50_NoRegister();
	A3_HENSHAWOLIVER_API UClass* Z_Construct_UClass_AWeaponBean50();
	A3_HENSHAWOLIVER_API UClass* Z_Construct_UClass_AWeapon();
	UPackage* Z_Construct_UPackage__Script_A3_HenshawOliver();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	void AWeaponBean50::StaticRegisterNativesAWeaponBean50()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(AWeaponBean50);
	UClass* Z_Construct_UClass_AWeaponBean50_NoRegister()
	{
		return AWeaponBean50::StaticClass();
	}
	struct Z_Construct_UClass_AWeaponBean50_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Body_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Body;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Sight_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Sight;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Barrel_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Barrel;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Can_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPropertyParams NewProp_Can;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AWeaponBean50_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AWeapon,
		(UObject* (*)())Z_Construct_UPackage__Script_A3_HenshawOliver,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWeaponBean50_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "WeaponBean50.h" },
		{ "ModuleRelativePath", "WeaponBean50.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWeaponBean50_Statics::NewProp_Body_MetaData[] = {
		{ "Category", "WeaponBean50" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "WeaponBean50.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWeaponBean50_Statics::NewProp_Body = { "Body", nullptr, (EPropertyFlags)0x0040000000080009, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWeaponBean50, Body), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWeaponBean50_Statics::NewProp_Body_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWeaponBean50_Statics::NewProp_Body_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWeaponBean50_Statics::NewProp_Sight_MetaData[] = {
		{ "Category", "WeaponBean50" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "WeaponBean50.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWeaponBean50_Statics::NewProp_Sight = { "Sight", nullptr, (EPropertyFlags)0x0040000000080009, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWeaponBean50, Sight), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWeaponBean50_Statics::NewProp_Sight_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWeaponBean50_Statics::NewProp_Sight_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWeaponBean50_Statics::NewProp_Barrel_MetaData[] = {
		{ "Category", "WeaponBean50" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "WeaponBean50.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWeaponBean50_Statics::NewProp_Barrel = { "Barrel", nullptr, (EPropertyFlags)0x0040000000080009, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWeaponBean50, Barrel), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWeaponBean50_Statics::NewProp_Barrel_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWeaponBean50_Statics::NewProp_Barrel_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWeaponBean50_Statics::NewProp_Can_MetaData[] = {
		{ "Category", "WeaponBean50" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "WeaponBean50.h" },
	};
#endif
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWeaponBean50_Statics::NewProp_Can = { "Can", nullptr, (EPropertyFlags)0x0040000000080009, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWeaponBean50, Can), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWeaponBean50_Statics::NewProp_Can_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWeaponBean50_Statics::NewProp_Can_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AWeaponBean50_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWeaponBean50_Statics::NewProp_Body,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWeaponBean50_Statics::NewProp_Sight,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWeaponBean50_Statics::NewProp_Barrel,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWeaponBean50_Statics::NewProp_Can,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AWeaponBean50_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AWeaponBean50>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_AWeaponBean50_Statics::ClassParams = {
		&AWeaponBean50::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AWeaponBean50_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AWeaponBean50_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AWeaponBean50_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AWeaponBean50_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AWeaponBean50()
	{
		if (!Z_Registration_Info_UClass_AWeaponBean50.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_AWeaponBean50.OuterSingleton, Z_Construct_UClass_AWeaponBean50_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_AWeaponBean50.OuterSingleton;
	}
	template<> A3_HENSHAWOLIVER_API UClass* StaticClass<AWeaponBean50>()
	{
		return AWeaponBean50::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(AWeaponBean50);
	struct Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_WeaponBean50_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_WeaponBean50_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_AWeaponBean50, AWeaponBean50::StaticClass, TEXT("AWeaponBean50"), &Z_Registration_Info_UClass_AWeaponBean50, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(AWeaponBean50), 1944465465U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_WeaponBean50_h_819377250(TEXT("/Script/A3_HenshawOliver"),
		Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_WeaponBean50_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_A3_HenshawOliver_Source_A3_HenshawOliver_WeaponBean50_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
