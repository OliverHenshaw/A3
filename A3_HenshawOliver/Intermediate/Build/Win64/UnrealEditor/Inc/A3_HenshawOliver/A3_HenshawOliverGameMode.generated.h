// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef A3_HENSHAWOLIVER_A3_HenshawOliverGameMode_generated_h
#error "A3_HenshawOliverGameMode.generated.h already included, missing '#pragma once' in A3_HenshawOliverGameMode.h"
#endif
#define A3_HENSHAWOLIVER_A3_HenshawOliverGameMode_generated_h

#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverGameMode_h_12_SPARSE_DATA
#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverGameMode_h_12_RPC_WRAPPERS
#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAA3_HenshawOliverGameMode(); \
	friend struct Z_Construct_UClass_AA3_HenshawOliverGameMode_Statics; \
public: \
	DECLARE_CLASS(AA3_HenshawOliverGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/A3_HenshawOliver"), A3_HENSHAWOLIVER_API) \
	DECLARE_SERIALIZER(AA3_HenshawOliverGameMode)


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAA3_HenshawOliverGameMode(); \
	friend struct Z_Construct_UClass_AA3_HenshawOliverGameMode_Statics; \
public: \
	DECLARE_CLASS(AA3_HenshawOliverGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/A3_HenshawOliver"), A3_HENSHAWOLIVER_API) \
	DECLARE_SERIALIZER(AA3_HenshawOliverGameMode)


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	A3_HENSHAWOLIVER_API AA3_HenshawOliverGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AA3_HenshawOliverGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(A3_HENSHAWOLIVER_API, AA3_HenshawOliverGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AA3_HenshawOliverGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	A3_HENSHAWOLIVER_API AA3_HenshawOliverGameMode(AA3_HenshawOliverGameMode&&); \
	A3_HENSHAWOLIVER_API AA3_HenshawOliverGameMode(const AA3_HenshawOliverGameMode&); \
public:


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	A3_HENSHAWOLIVER_API AA3_HenshawOliverGameMode(AA3_HenshawOliverGameMode&&); \
	A3_HENSHAWOLIVER_API AA3_HenshawOliverGameMode(const AA3_HenshawOliverGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(A3_HENSHAWOLIVER_API, AA3_HenshawOliverGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AA3_HenshawOliverGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AA3_HenshawOliverGameMode)


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverGameMode_h_9_PROLOG
#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverGameMode_h_12_SPARSE_DATA \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverGameMode_h_12_RPC_WRAPPERS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverGameMode_h_12_INCLASS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverGameMode_h_12_SPARSE_DATA \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverGameMode_h_12_INCLASS_NO_PURE_DECLS \
	FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> A3_HENSHAWOLIVER_API UClass* StaticClass<class AA3_HenshawOliverGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_A3_HenshawOliver_Source_A3_HenshawOliver_A3_HenshawOliverGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
